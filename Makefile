build:main entr wws
	gcc main.o entr.o wws.o -o phone2search

debag:main_debag entr_debag wws_debag
	gcc main.o entr_d.o wws_d.o -g3 -o phone2search_degag

main:main.c
	gcc -c main.c

entr:check_enterence.c
	gcc -c check_enterence.c -o entr.o

wws:work_with_string.c
	gcc -c work_with_string.c -o wws.o

main_debag:main.c
	gcc -c main.c -g

entr_debag:check_enterence.c
	gcc -c check_enterence.c -g3 -o entr_d.o

wws_debag:work_with_string.c
	gcc -c work_with_string.c -g3 -o wws_d.o

clean:
	rm -rf ./*.o

help:
	echo -e '\t[ Help menu ]\nbuild\t\t----\t\tbuild the project\ndebag\t\t----\t\tbuild the project for debug\nclean\t\t----\t\tremove all build files\nhelp\t\t----\t\tshow this menu\n'