#include <stdio.h>

#define bool char
#define true 1
#define false 0

int lenstr(char str[]);
bool find_number(char symbol);
bool check_phone_number(char number[]);
