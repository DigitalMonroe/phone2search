#include "work_with_string.h"

void copy_str(char str1[], char str2[])
{
	for(char i = 0 ;str1[i] = str2[i]; ++i);
}

//takes number, position and adding symbol
void add_symbol(char str[], char position, char symbol)
{
	char tmp_str[20] = {0};
	for(char i = 0, j = 0;str[i]; ++i, ++j)
	{
		if(i == position)
		{
			tmp_str[j] = symbol;
			++j;
		}
		tmp_str[j] = str[i];
	}
	copy_str(str, tmp_str);
}
