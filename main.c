#include "check_enterence.h"
#include "work_with_string.h"


void main(int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("Usage: %s <phone_number>\nExample: %s 71234567890\n", argv[0], argv[0]);
		return;
	}
	
	char *original_number = argv[1];

	if(check_phone_number(original_number) == true)
		return;
	
	char temp_str[20] = {0};
	copy_str(temp_str, original_number);
	printf("\"%s\" | ", temp_str); //71234567890
	
	temp_str[0] = '8';
	printf("\"%s\" | ", temp_str); //871234567890
	temp_str[0] = '7';
	
	add_symbol(temp_str, 1, ' ');
	add_symbol(temp_str, 5, ' ');
	printf("\"%s\" | ", temp_str); //7 123 4567890

	temp_str[0] = '8';
	printf("\"%s\" | ", temp_str); //8 123 4567890
	temp_str[0] = '7';

	add_symbol(temp_str, 9, ' ');
	add_symbol(temp_str, 12, ' ');
	printf("\"%s\" | ", temp_str); //7 123 456 78 90

	temp_str[0] = '8';
	printf("\"%s\" | ", temp_str); //8 123 456 78 90

	copy_str(temp_str, original_number);

	add_symbol(temp_str, 1, '.');
	printf("\"%s\" | ", temp_str); //7.1234567890
	
	copy_str(temp_str, original_number);
	
	temp_str[0] = '8';
	add_symbol(temp_str, 1, '(');
	add_symbol(temp_str, 5, ')');
	printf("\"%s\" | ", temp_str); //8(123)4567890

	printf("\"%s\" |", temp_str+1); //(123)4567890

	copy_str(temp_str, original_number);

	add_symbol(temp_str, 1, '(');
	add_symbol(temp_str, 5, ')');
	add_symbol(temp_str, 9, '-');
	add_symbol(temp_str, 12, '-');
	printf("\"%s\" | ", temp_str); //7(123)456-78-90

	temp_str[0] = '8';
	printf("\"%s\"\n", temp_str); //8(123)456-78-90
}
